var io = require('socket.io').listen(5000);

io.on('connection', function(socket){

    socket.on('join:room', function(data){
        var room_name = data.room_name;
        socket.join(room_name);
    });


    socket.on('leave:room', function(msg){
        msg.text = msg.user + " has left the room";
        socket.in(msg.room).emit('exit', msg);
        socket.leave(msg.room);
    });


    socket.on('send:message', function(msg){
        socket.in(msg.room).emit('message', msg);
    });
});


// var express = require('express');
// var app = express();
// var server = require('http').createServer(app);
// var io = require('socket.io')().listen(server);
// io.set('origins', 'http://192.168.1.254:8100:*');

// io.on("connection", function(socket){
//     // console.log("A new client has connected with the id " + socket.id + "!");
    
//     // socket.on("disconnect", function(){
//     //     console.log("the client has disconnected!");
//     // });
    
//     // socket.on("Message", function(data){
//     //     console.log(data.message);
        
//     //     io.emit("Message", data)
        
//     // });
    
//     // socket.on("Private Message", function(data){
//     //     console.log(data);
        
//     //     io.emit(data.chatID, data)
        
//     // });
    
  
    
//     // socket.on('typing', function(data){
//     //     io.emit(data.chatID+'-typing', data);
//     // });
    
//     // socket.on('stop typing', function(data){
//     //     io.emit(data.chatID+'-stop typing', data);
//     // });
//     socket.on('join:room', function(data){
//         var room_name = data.room_name;
//         socket.join(room_name);
//     });


//     socket.on('leave:room', function(msg){
//         msg.text = msg.user + " has left the room";
//         socket.in(msg.room).emit('exit', msg);
//         socket.leave(msg.room);
//     });


//     socket.on('send:message', function(msg){
//         socket.in(msg.room).emit('message', msg);
//     });
// })

// var PORT = process.env.PORT || 3000;
// server.listen(PORT, function(){
//     console.log("Listening on PORT " + process.env.PORT);
// })

// app.all('/', function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "X-Requested-With");
//   next();
//  });
 
//  /*
// var PORT = process.env.OPENSHIFT_NODEJS_PORT || 3000;
// var IP = process.env.OPENSHIFT_NODEJS_IP;
// server.listen(PORT,IP, function(){
//     console.log("Listening on PORT " + PORT);
// });*/



// var express = require('express');
// var app = express();
// var server = require('http').createServer(app);
// var io = require('socket.io')().listen(server);

// io.on("connection", function(socket){
//     console.log("A new client has connected with the id " + socket.id + "!");
    
//     socket.on("disconnect", function(){
//         console.log("the client has disconnected!");
//     });
    
//     socket.on("Message", function(data){
//         console.log(data.message);
        
//         io.emit("Message", data)
        
//     });
    
//     socket.on("Private Message", function(data){
//         console.log(data);
        
//         io.emit(data.chatID, data)
        
//     });
    
  
    
//     socket.on('typing', function(data){
//         io.emit(data.chatID+'-typing', data);
//     });
    
//     socket.on('stop typing', function(data){
//         io.emit(data.chatID+'-stop typing', data);
//     });
// })

// var PORT = process.env.PORT || 3000;
// server.listen(PORT, function(){
//     console.log("Listening on PORT " + process.env.PORT);
// })
 
//  /*
// var PORT = process.env.OPENSHIFT_NODEJS_PORT || 3000;
// var IP = process.env.OPENSHIFT_NODEJS_IP;
// server.listen(PORT,IP, function(){
//     console.log("Listening on PORT " + PORT);
// });*/